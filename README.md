# Nest Custom Integration


### Custom Integration with Payment Gateway
* [Stripe Payment](https://gitlab.com/fv.david/nest-custom-payment/-/tree/stripe-payment)

### Custom Integration with Twilio
* [Twilio Whatsapp Notification](https://gitlab.com/fv.david/nest-custom-payment/-/tree/twilio)

